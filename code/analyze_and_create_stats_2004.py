# Analyze TIP 2004 papers

import pandas as pd
import requests
from tqdm import tqdm
import time
from scipy import stats

# Filenames
filename = '../data/TIP_2004_papers_update.xlsx'
filename_citationupdate = '../data/export2019.05.14-07.59.33.csv'

# load data
df = pd.read_excel(filename, header=0)
papers = df[pd.notna(df['DOI'])].copy()
df2 = pd.read_csv(filename_citationupdate, header=0)

# merge tables (updating citation counts)
papers['DOI'] = papers['DOI'].str.strip()
df2['DOI'] = df2['DOI'].str.strip()
papers_updatedcitations = papers.merge(df2[['DOI', 'Article Citation Count']],
         on='DOI',
         how='left',
         suffixes=['_old', ''])
papers_updatedcitations['Article Citation Count'].fillna(value=0, inplace=True)
papers_updatedcitations['Code Link'].fillna(value=0, inplace=True)

# split between papers with and without code available online
papers_withcode = papers_updatedcitations[papers_updatedcitations['Code Link'] != 0]
papers_nocode = papers_updatedcitations[papers_updatedcitations['Code Link'] == 0]

# Calculate statistics
col_cit = 'Article Citation Count'
citations_withcode_mean = papers_withcode[col_cit].mean()
citations_withcode_median = papers_withcode[col_cit].median()
citations_nocode_mean = papers_nocode[col_cit].mean()
citations_nocode_median = papers_nocode[col_cit].median()

[statistics, p] = stats.mannwhitneyu(papers_withcode[col_cit], 
                   papers_nocode[col_cit], 
                   use_continuity=True, 
                   alternative='two-sided')

# print results
print('papers without code: found', papers_nocode.shape[0],'/', papers.shape[0], ' (',
      round(100*papers_nocode.shape[0]/papers.shape[0]),'%):',
      'citations mean',"%6.2f" %(citations_nocode_mean),'median',citations_nocode_median)
print('papers with code   : found', papers_withcode.shape[0],'/', papers.shape[0], ' (',
      round(100*papers_withcode.shape[0]/papers.shape[0]),'%):',
      'citations mean',"%6.2f" %(citations_withcode_mean),'median',citations_withcode_median)
print('Mann-Whitney U-test: hypothesis H0 that medians are the same rejected with p =',"%.2e" %(p))
