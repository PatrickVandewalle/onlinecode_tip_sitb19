# Analyze TIP 2017 papers

import pandas as pd
import requests
from tqdm import tqdm
import time
from scipy import stats

# filenames
filename = '../data/TIP_2017.xlsx'
filename_citationupdate = '../data/export2019.05.13-09.18.17.csv'
filename_OA = '../data/results.csv'

# load data
df = pd.read_excel(filename, header=1)
papers = df[pd.notna(df['DOI'])].copy()
df2 = pd.read_csv(filename_citationupdate, header=0)
dfOA = pd.read_csv(filename_OA, header=0)

# merge tables (updating citation counts)
papers['DOI'] = papers['DOI'].str.strip()
df2['DOI'] = df2['DOI'].str.strip()
dfOA['DOI'] = dfOA['DOI'].str.upper()
dfOA['DOI'] = dfOA['DOI'].str.strip()
papers_updatedcitations = papers.merge(df2[['DOI', 'Article Citation Count']],
         on='DOI',
         how='left',
         suffixes=['_old', ''])
papers_updatedcitations = papers_updatedcitations.merge(dfOA[['DOI', 'best_oa_url']],
                                       on='DOI',
                                       how='left',
                                       suffixes=['_old', ''])
papers_updatedcitations['Article Citation Count_old'].fillna(value=0, inplace=True)
papers_updatedcitations['Article Citation Count'].fillna(value=0, inplace=True)
papers_updatedcitations['best_oa_url'].fillna(value=0, inplace=True)

# split between papers with and without code available online
papers_withcode = papers_updatedcitations[papers_updatedcitations['Code Link'] != 0]
papers_nocode = papers_updatedcitations[papers_updatedcitations['Code Link'] == 0]
papers_OA = papers_updatedcitations[papers_updatedcitations['best_oa_url'] != 0]
papers_nonOA = papers_updatedcitations[papers_updatedcitations['best_oa_url'] == 0]
papers_withcode_OA = papers_withcode[papers_withcode['best_oa_url'] != 0]

# Calculate statistics
col_cit = 'Article Citation Count'
citations_withcode_mean = papers_withcode[col_cit].mean()
citations_withcode_median = papers_withcode[col_cit].median()
citations_nocode_mean = papers_nocode[col_cit].mean()
citations_nocode_median = papers_nocode[col_cit].median()
citations_OA_mean = papers_OA[col_cit].mean()
citations_OA_median = papers_OA[col_cit].median()
citations_nonOA_mean = papers_nonOA[col_cit].mean()
citations_nonOA_median = papers_nonOA[col_cit].median()

[statistics, p] = stats.mannwhitneyu(papers_withcode[col_cit], 
                   papers_nocode[col_cit], 
                   use_continuity=True, 
                   alternative='two-sided')
[statisticsOA, pOA] = stats.mannwhitneyu(papers_OA[col_cit],
                                     papers_nonOA[col_cit],
                                     use_continuity=True,
                                     alternative='two-sided')

# print results
print('Open Access')
print('non-OA papers: found', papers_nonOA.shape[0],'/', papers.shape[0], ' (',
      round(100*papers_nonOA.shape[0]/papers.shape[0]),'%):',
      'citations mean',"%6.2f" %(citations_nonOA_mean),'median',citations_nonOA_median)
print('OA papers    : found', papers_OA.shape[0],'/', papers.shape[0], ' (',
      round(100*papers_OA.shape[0]/papers.shape[0]),'%):',
      'citations mean',"%6.2f" %(citations_OA_mean),'median',citations_OA_median)
print('Mann-Whitney U-test: hypothesis H0 that medians are the same rejected with p =',"%.2e" %(pOA))
print('code availability')
print('papers without code: found', papers_nocode.shape[0],'/', papers.shape[0], ' (',
      round(100*papers_nocode.shape[0]/papers.shape[0]),'%):',
      'citations mean',"%6.2f" %(citations_nocode_mean),'median',citations_nocode_median)
print('papers with code   : found', papers_withcode.shape[0],'/', papers.shape[0], ' (',
      round(100*papers_withcode.shape[0]/papers.shape[0]),'%):',
      'citations mean',"%6.2f" %(citations_withcode_mean),'median',citations_withcode_median)
print('Mann-Whitney U-test: hypothesis H0 that medians are the same rejected with p =',"%.2e" %(p))
print('papers with code that are open access:', papers_withcode_OA.shape[0], '(',
      "%4.2f" %(100*papers_withcode_OA.shape[0]/papers_withcode.shape[0]),'% of papers with code)')
