%% read and analyze 2017 data
clear all
[ndata, text, alldata] = xlsread('../data/TIP_2017.xlsx');

n = 0;
for i=1:size(alldata,2)
    tmp1(i) = strcmp(alldata{2,i},'Article Citation Count');
    tmp2(i) = strcmp(alldata{2,i},'Code Link');
end
col_citations = find(tmp1);
col_code = find(tmp2);
clear tmp1 tmp2;
for i=3:size(alldata,1) % rows 1 and 2 contain titles
    if (length(alldata{i,1})>1)
        n = n+1;
        citations{2017}(n) = alldata{i,col_citations};
        code{2017}(n) = length(alldata{i,col_code})>1;
    end
end
citations{2017}(isnan(citations{2017})) = 0;

citations_sorted_code0 = sort(citations{2017}(code{2017}==0),'descend');
citations_sorted_code1 = sort(citations{2017}(code{2017}==1),'descend');
disp(['found ' num2str(length(citations_sorted_code1)) '/' num2str(length(citations_sorted_code0)+length(citations_sorted_code1)) ...
    ' papers with code: ' num2str(100*length(citations_sorted_code1)/(length(citations_sorted_code0)+length(citations_sorted_code1))) '%']);

disp(['citations for papers without code: mean ' num2str(mean(citations_sorted_code0)) '; median ' num2str(median(citations_sorted_code0))]);
disp(['citations for papers with    code: mean ' num2str(mean(citations_sorted_code1)) '; median ' num2str(median(citations_sorted_code1))]);
disp(['TEST: ' num2str(sum(citations_sorted_code1))])
%% generate plots
% plot variables
fontsize = 12;

figure;
fig_handle = boxplot(citations{2017},code{2017},'Labels',{'no code','code'}); 
title('citations 2017');
set(fig_handle(:,:),'linewidth',2);
ylabel('number of citations','FontSize',fontsize);

%% review links for earlier studied 2004-2006 papers
for year = 2004:2006
    disp(['analyzing year ' num2str(year)]);
    % read data
    [ndata, text, alldata] = xlsread(['../data/TIP_' num2str(year) '_papers_update.xlsx']);
    % first determine column containing citations and links
    for i=1:size(alldata,2)
        tmp1(i) = strcmp(alldata{1,i},'citations');
        tmp2(i) = strcmp(alldata{1,i},'Code Link');
    end
    col_citations = find(tmp1);
    col_code = find(tmp2);
    clear tmp1 tmp2;
    % then analyze data
    for i=3:size(alldata,1)
        citations{year}(i-2) = alldata{i,col_citations};
        code{year}(i-2) = length(alldata{i,col_code})>1;
    end
    disp(['found code for ' num2str(sum(code{year})) '/' num2str(length(code{year})) ' papers in ' num2str(year)]);
end
disp(['Summary: found code for ' num2str(sum(code{2004})+sum(code{2005})+sum(code{2006})) ...
    '/' num2str(length(code{2004})+length(code{2005})+length(code{2006})) ' papers in 2004-2006']);

%% statistical tests: checking whether the median number of citations for
%  papers with code online is significantly different from the other
%  papers, using the Mann-Whitney U-test
for y = [2004 2005 2006 2017]
    [p(y),h(y)] = ranksum(citations{y}(code{y}==0),citations{y}(code{y}==1));
% disp([num2str(y) ': null hypothesis rejected ' num2str(h(y)) ', p = ' num2str(p(y))]);
end

%% summarize results
disp(' '); % empty line
disp('Summary: ');
for y = [2004 2005 2006 2017]
    disp([num2str(y) ': ' num2str(length(code{y})) ' papers, ' ...
        num2str(sum(code{y}==1)) ' with code (' ...
        num2str(100*sum(code{y}==1)/length(code{y})) '%), ']);
    disp(['        citations for papers without code avg ' ...
        num2str(mean(citations{y}(code{y}==0)),'%3.2f') ', median ' ...
        num2str(median(citations{y}(code{y}==0)),'%3.2f') '; ']);
    disp(['        citations for papers with code    avg ' ...
        num2str(mean(citations{y}(code{y}==1)),'%3.2f') ', median ' ...
        num2str(median(citations{y}(code{y}==1)),'%3.2f') '; ' ...        
        ]);
    disp(['        null hypothesis rejected ' num2str(h(y)) ...
        ', p = ' num2str(p(y))]);
end