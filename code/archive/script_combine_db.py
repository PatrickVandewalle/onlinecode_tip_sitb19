import pandas as pd
import requests
from tqdm import tqdm
import time

filename = '../data/TIP_2017.xlsx'
paper_col = 'export2019.04.09-03.54.48'
output_file = 'output.xlsx'

df = pd.read_excel(filename)
papers = df[paper_col]
repo_urls = []

for p in tqdm(papers.values):
    payload = {
        'q': f'"{p}"+in:readme',
    }
    r = requests.get('https://api.github.com/search/repositories',
                     params=payload)
    if (r.status_code == 200) and len(r.json()['items']) > 0:
        repo = r.json()['items'][0]
        tqdm.write(f'Paper: {p} | URL: {repo["html_url"]}')
        repo_urls.append(repo['html_url'])
    elif r.status_code != 200:
        tqdm.write(f'Request failed with status code "{r.status_code}"')
        repo_urls.append(0)
    else:
        repo_urls.append(0)

    time.sleep(6)

df['Repo'] = repo_urls
df.to_excel(output_file)
