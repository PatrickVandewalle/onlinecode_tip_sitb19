\documentclass[conference]{latex/IEEEtran}
%\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage{hyperref}
\usepackage{booktabs}

\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}

% prettier tables (also add booktabs package)
\renewcommand{\arraystretch}{1.2}
\newcommand{\ra}[1]{\renewcommand{\arraystretch}{#1}}

\begin{document}

\title{Code availability for image processing papers: \\ a status update
%\thanks{Identify applicable funding agency here. If none, delete this.}
}

\author{\IEEEauthorblockN{Patrick Vandewalle}
\IEEEauthorblockA{\textit{EAVISE, Dept. of Electrical Engineering (ESAT)} \\
\textit{KU Leuven}\\
Sint-Katelijne-Waver, Belgium \\
Patrick.Vandewalle@kuleuven.be}
}

\maketitle

\begin{abstract}
Reproducing computational results described in a publication often requires more than only the article itself: code and data are also needed to reproduce these results. Code availability is analyzed here for articles published in IEEE Transactions on Image Processing in 2017, and compared to an earlier study covering the same journal for 2004-2006. Approximately a quarter of the publications from 2017 have code available online, compared to less than 10\% in our earlier study. On average, papers with code available online get cited about twice as often, providing a clear incentive for sharing code more frequently.
\end{abstract}

\begin{IEEEkeywords}
reproducible research, open science, open source, code availability, impact
\end{IEEEkeywords}

\section{Introduction}
When publishing our work, we want to share our research and results with the broader research community. This brings the current state of the art forward, and allows others to build further upon our work. When our work consists in a theoretical analysis, such as the proof of a theorem, the paper as a combination of text and equations is usually sufficient for others to verify and repeat those results. However, when writing a paper about computational results, such a paper description rarely allows other researchers to reproduce the same results. There is a lot of additional information such as initial conditions, simulation environment, input data, etc. that is very difficult (if not impossible) to fit into the constraints of a research publication. 

In order to make our computational results reproducible, it is therefore often essential to also share data and code together with the actual publication. Ten years ago, J. Kova\v{c}evi\'{c}, M. Vetterli and I wrote a paper about reproducible research in signal processing~\cite{VandewalleKV09}. This paper provides a status update, in particular with respect to code availability: where are we now? 

Next to simply being good practice, making code and data available to improve reproducibility of a publication also offers another advantage: it is often linked to an increased impact of the work. Articles with code and/or data available online, or even where the article itself is openly made available online (commonly called `open access') are said to be cited more often than their counterparts sharing less information. 

\begin{table*}[!h]
\caption{Summary statistics}
\centering
\ra{1.3}
\begin{tabular}{@{}lllll@{}}\toprule
						& 2004 	& 2005 	& 2006 	& 2017  \\ \midrule
total number of papers 		& 134 	& 182 	& 329 	& 450 \\
papers with code 			& 10 (7\%)& 12 (7\%)& 25 (8\%)& 108 (24\%) \\
citations papers without code & & && \\
\quad \quad mean 			& 79.7	& 65.8	& 54.0	& 8.5 \\
\quad \quad median 			& 36	 	& 35	 	& 27 		& 4 \\
citations papers with code &  & && \\
\quad \quad mean 			& 1430.2	& 292.7 	& 233.4 	& 17.1 \\
\quad \quad median 			& 124 	& 120.5	& 131	& 9.5 \\
U-test p value 				& 5.0e-2 	& 5.1e-3 	& 3.8e-6 	& 8.6e-6 \\ 
\bottomrule
\end{tabular}
\label{table:stats}
\end{table*}

\section{Related work}
A set of studies related to this work have been performed in different domains. In many of these studies, the focus is more on data availability than explicitly on code availability. In some of them the term `data' is used to cover both measurement data and the code to analyze it. Piwowar et al.\ analyzed 85 cancer microarray clinical trial publications and found a clear citation advantage (of 69\%) for the papers using publicly available microarray data~\cite{PiwowarDF07}. In a much larger follow-up study in 2013, Piwowar and Vision analyzed 10555 studies on gene expression microarray data~\cite{PiwowarV13}. Taking a broad set of covariates into account such as publication date, journal impact factor, open access status, first and last author publication history, they found a 9\% citation advantage for publications with online data. Drachen et al.\ found a significant citation advantage for papers in astrophysical journals sharing data~\cite{DrachenELD16}. Henneken and Accomazzi analyzed astronomy articles sharing data, and concluded that on average, articles with data links received 20\% more citations compared to articles without data links~\cite{HennekenA11}. Data and code sharing policies of 170 journals in the ISI Web of Knowledge fields of ``mathematical \& computational biology'', ``statistics and probability'' and ``multidisciplinary sciences'' were analyzed by Stodden et al.~\cite{StoddenGM13}.  In 2012, 38\% of these journals had a data policy, and 22\% had a code policy. In another study, Stodden et al. tested the code and data sharing policy for Science, a high-impact journal~\cite{StoddenSM18}. Combining a web search for code and data with contacting the authors, they obtained artifacts (code and/or data) for 44\% of the analyzed articles, and were able to reproduce results for 26\%. 

On a related matter, a citation advantage is also often found for papers that are openly available online themselves: the open access citation advantage. This has been studied first in a large study by Lawrence~\cite{Lawrence01}, followed by a large number of studies in various disciplines. An overview of studies until 2010 is made by Swan~\cite{Swan10}, and more bibliographies on the topic are available online. Piwowar et al.\ have made a large-scale study on the state of open access in 2018 using the free oaDOI service, checking open access status for about 67 million articles~\cite{Piwowaretal18}. They conclude that at least 28\% of those are open access, and that this proportion is growing (indicated by higher percentages for the most recent literature). In terms of increased impact, they found that open access papers receive 18\% more citations than average. 

\section{Current status}
I analyzed articles published in IEEE Transactions on Image Processing (TIP) in 2017, and compared these results to those from our previous study on TIP 2004-2006. Code and data used for this analysis are available online at \hyperlink{https://gitlab.com/PatrickVandewalle/onlinecode_tip_sitb19}{https://gitlab.com/PatrickVandewalle/onlinecode\_tip\_sitb19}.

A total of 450 regular papers were published in Transactions on Image Processing in 2017, excluding comments, corrections and editorials. 
For each paper, a web search was performed for the paper title (between quotes), in combination with the term ``source''. The first and second page of results were manually analyzed searching for links to the code. Additionally, a script was run to search code for papers on Github as that is clearly one of the popular locations for source code. 

I found code available online for 108 papers, or 24\%. This has more than doubled compared to the 9\% found for the papers from 2004~\cite{VandewalleKV09}, and 10\% found when analyzing 2004-2006 later~\cite{Vandewalle12}. Typical locations for such source code are \hyperlink{https://github.com}{GitHub} repositories (65 of the 108 links found) and personal web pages. 

Since early 2017, IEEE offers authors the possibility to share code related to their publication via the IEEE platform through Code Ocean~\cite{Ayala17}. Code Ocean is not only a platform for sharing research code, it also allows visitors to run the code or parts of it using a cloud service without having to download or install anything. So far, this feature is only used by a small minority of TIP papers: 4 papers from 2017 make use of this feature, 2 papers from 2018, and so far two paper from 2019 (current ongoing year). Seven papers published prior to 2017 include code via Code Ocean.

For these 450 papers, I also checked whether the publication itself is open access: is it freely available online? This analysis was performed using the oaDOI service. Using this method, I found open access versions of 141 papers (31\%), while only 18 papers are flagged on the IEEE Xplore website as (paid) open access. Only 42 of the papers with code are available in open access (39\%), so it seems that code availability and open access status are not directly linked.

\section{Citation advantage}
When looking at the citations of those papers (as measured on IEEE Xplore), we can see that papers in our test set (published in 2017) with code available online have an average of 17.1 and a median of 9.5 citations, compared to an average of 8.5 and a median of 4 citations for papers with no code available. This shows a correlation between code availability and number of citations, with on average approximately twice as many citations for papers having code available compared to the papers having no code available online. We ran a two-sided Mann-Whitney U-test to verify the significance of this difference. It shows that the null hypothesis (which states that sets of citations with and without code have the same median) can be rejected with p-value $8.6*10^{-6}$. 

The analyzed year 2017 was chosen to be a recent year because of rapidly changing practices in making code available online. However, a disadvantage of choosing such a recent year is that it is still very early to analyze citations. Citation rates for publications typically show a peak after a few years, which may not have been reached for the analyzed publications. Also, the large relative time difference between a publication made in January 2017 or December 2017 may have a big impact on the citation numbers. I performed my analysis under the assumption that these effects will equally affect papers with and without code available. 

Next to these analyzed papers from 2017, I also updated the links and citations for the papers from 2004 to 2006 analyzed earlier~\cite{Vandewalle12}. I did not search for new links, but checked the validity of the links found earlier. If the link was not valid anymore (and straightforward fixes did not work), I removed it (although a link to code has been available for this publication for an unknown period). Unfortunately, this was the case for 19 of the links found earlier. Citations were updated using the citation counts on the IEEE Xplore webpage. Results are summarized in Table~\ref{table:stats}, and show similar behavior as for 2017 (but now with higher overall citation numbers). 

Although all the data consistently show more citations for papers having code available online, please note that causality of this relation can only be determined through a large scale controlled experiment where other control parameters are taken into account, such as the number and seniority of authors, home institution, etc. This is beyond the scope of the current analysis.

\section{Discussion}
In this work, I focused on online availability of code. I did not check explicitly for availability of data or data sets. Both are of course crucial in any computational research publications. While there is a clear increase in papers providing code from 9\% in 2004 to 24\% in 2017, this still represents only a minority of the published articles in that year. We still have a way to go before this is common practice.

Of course, there may be a variety of reasons for not making code available. In industrial projects, the company or contract often does not allow public sharing of the code (if publishing is accepted at all). When dealing with medical or privacy-sensitive data, the data can often also not be shared directly. Sometimes anonymization can be a solution in those situations. 

Next to these legal and privacy-related issues, there are also some technical hurdles. The average lifetime of a web page is a concern when making additional information such as code available. Out of the 66 links to code found in 2012, only 47 were still valid (or easy to fix). Hopefully some of the recently developed and currently popular platforms such as GitHub, Code Ocean, PapersWithCode.com and institutional repositories can guarantee a longer lifetime for future publications.

Searching open access versions of the articles was performed using the freely available oaDOI service (using the same underlying database as the Unpaywall plugin). While it does seem to miss some openly available articles (Piwowar et al.\ report a recall of 77\% in their test~\cite{Piwowaretal18}), this service offers an easy and convenient method for checking online open access availability of papers. Unfortunately such a system does not exist (yet?) for code and/or data related to a publication, so the analysis performed for this study required a lot of time-consuming manual web searches. 

\section*{Acknowledgment}
I would like to thank Floris De Feyter for helping with the code searching by writing a script for searching Github repositories. 

\bibliographystyle{ieeetr}
\bibliography{./references}

\end{document}
